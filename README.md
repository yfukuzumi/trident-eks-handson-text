# 目次 <!-- omit in toc -->
- [ハンズオン環境](#ハンズオン環境)
  - [注意事項](#注意事項)
- [AWS環境へのログイン](#aws環境へのログイン)
- [CloudShellの利⽤](#cloudshellの利)
- [EKSの確認](#eksの確認)
- [ONTAPの確認](#ontapの確認)
- [Worker Nodeでの下準備](#worker-nodeでの下準備)
  - [Worker NodeへのNFSクライアントのインストール](#worker-nodeへのnfsクライアントのインストール)
  - [Worker NodeでのiSCSI設定](#worker-nodeでのiscsi設定)
- [Tridentインストール](#tridentインストール)
  - [Helmの導入](#helmの導入)
  - [Tridentの入手](#tridentの入手)
  - [ONTAPストレージの確認](#ontapストレージの確認)
  - [Kubernetes（EKS）内からのアクセス確認](#kuberneteseks内からのアクセス確認)
    - [ノードのIPからデータLIFへの疎通確認](#ノードのipからデータlifへの疎通確認)
    - [Podの中から管理LIFへの疎通確認](#podの中から管理lifへの疎通確認)
  - [Trident Operatorによる導入](#trident-operatorによる導入)
  - [Tridentインストールの確認](#tridentインストールの確認)
- [Tridentの設定（NAS前提）](#tridentの設定nas前提)
- [StorageClassの設定](#storageclassの設定)
- [PersistentVolumeの利用](#persistentvolumeの利用)
  - [ONTAP側から確認](#ontap側から確認)
- [Snapshotの利用](#snapshotの利用)
    - [VolumeSnapshotClassを作成](#volumesnapshotclassを作成)
    - [Sanpshotを作成](#sanpshotを作成)
    - [Snapshotの確認](#snapshotの確認)
- [iSCSIでの利用](#iscsiでの利用)
  - [ONTAP側のiSCSI設定と確認](#ontap側のiscsi設定と確認)
  - [TridentでのiSCSI設定](#tridentでのiscsi設定)
  - [iSCSI用PVCとPodの作成](#iscsi用pvcとpodの作成)
- [後片付け](#後片付け)


***

<a name="handason"></a>
# ハンズオン環境  
本トレーニングでは以下環境を利⽤してAmazon Fsx for NetApp ONTAPとAstra Tridentを試します。  

<img src="img/t0.png" width="80%">  
<br>

※トレーニングの都合上、各種リソースをPublic Subunetに配置しています。実際の環境ではセキュリティ上ありえない構成ですが、ご容赦ください。
## 注意事項
本トレーニングは、TrindentとONTAPの連携をとりあえず試してみることを目的としています。AWS/Kubernetesのベストプラクティスやセキュリティなどは一切考慮していませんのでご了承ください。また細かい説明は割愛していますので適宜マニュアル等の[公式ドキュメント](https://docs.netapp.com/us-en/trident/index.html)をご参照ください。また、こちらの[Blog](https://licensecounter.jp/devops-hub/blog/trident/)もご参照ください。

AWS実環境を利⽤したトレーニングのため、費⽤が発⽣します。トレーニングにかかる費用はネットワールドが負担しますが、無用な費用がかからないよう十分ご注意ください。  
***

<a name="awslogin"></a>
# AWS環境へのログイン  

1. メールに記載されているURLにアクセス  
   https://アカウントID.signin.aws.amazon.com/console/
<img src="img/p3-1.png" width="60%">  
<br>

   - アカウント※基本的には⼊⼒済み  
   - ユーザー名  
   - パスワード  
   を⼊⼒し、「**サインイン**」をクリック
<br>

2. AWSのコンソール画面へ  
<img src="img/p3-2.png" width="60%">  
<br>
***

<a name="cloudshell"></a>
# CloudShellの利⽤  
EKSの操作はCloudShellから実⾏します。  
<img src="img/p4-1.png" width="60%">  
<br>
1. リージョンが「**東京**」になっていることを確認
2. 「**CloudShell**」ボタンをクリック

<img src="img/p4-2.png" width="60%">  
<br>

3. /home/cloudshell-userに下記フォルダ／ファイルを保存しています。  

   ```
   •/ trident-eks-handson
     •trident-handson-eks.yml
     •trident-backend-nas.yml
     •trident-backend-san.yml   
     •storageclass-nas.yml  
     •storageclass-san.yml  
     •pod-iscsi.yml    
     •pvc-nas.yml  
     •pvc-iscsi.yml  
     •snap-sc.yml    
     •snap.yml    
     •README.md
   ```

4. /trident-eks-handsonに移動します。  
   ```cd trident-eks-handson```  
   特に指示がない限り、/ trident-eks-handsonで作業します。ディレクトリを移動した場合は適宜コマンドを修正してください。

5. CloudShellには以下のツールがインストール済みです。
   - eksctl  
   - kubectl  
※kubectl completionはインストールされていません　　



6. eksctlのバージョン確認  
   ```
   eksctl version
   ```  
※万が一eksctlがインストールされていない場合は以下コマンドを入力してください。
```
curl -L "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp
sudo mv /tmp/eksctl /usr/local/bin
eksctl version
```
7. kubectlの確認  
   ```
   kubectl version --client
   ```  
***

<a name="createeks"></a>
# EKSの確認
作成済みのEKSを確認します。

1. EKS Clusterの確認  
作成済みのclusterを確認します。  
```
eksctl get cluster
kubectl version
kubectl get svc
kubectl get node
```

***
# ONTAPの確認
作成済みのFSx for NetApp ONTAPを確認します。  
※ONTAP CLIはCloud9から実行します。これ以降、  
- EKSの操作：**CloudShell**
- ONTAPの操作：**Cloud9**  
- Worker Nodeの操作：**Sessiom Manager**  
となりますのでご注意ください。

1. 検索窓に「**Cloud9**」と入力し、サービス＞**Cloud9**をCtrlキーを押しながらクリックして別タブとして開きます。  
<img src="img/t1.png" width="70%">
<br>

1. 「**開く**」をクリック  
<img src="img/t2.png" width="70%">
<br>

1. 画面下部の赤枠で囲ったターミナル部分を利用します。見やすいようにバーを上に拡大してください。
<img src="img/t3.png" width="70%">
<br>  


4. 下記コマンドでONTAP CLIへ接続します。
```
ssh fsxadmin@[cluster management LIFのIP]
```
cluster management LIFのIP/Passwordはメールを確認してください。AWSマネジメントコンソール>FSx>ファイルシステム＞ファイルシステムID＞エンドポイント＞管理エンドポイント -IP アドレスでも確認できます。  

5. ONTAPの基本的な情報を下記コマンドで確認します。
```
rows 0
cluster show
vserver show
network interface show
storage aggregate show
volume show
```
# Worker Nodeでの下準備
## Worker NodeへのNFSクライアントのインストール
通常はWorker NodeへNFSクライアントのインストールが必要ですが、EKS環境ではインストール済みのため今回は対応不要です。

## Worker NodeでのiSCSI設定
AWSマネジメントコンソールの**セッションマネージャー**からWorker NodeにログインしてiSCSI設定をします。

1. AWSマネジメントコンソール＞EC2>インスタンス＞インスタンス＞trident-eksctl-trident-eksctl-nodegroups-Nodeにチェックを入れて「**接続**」をクリック  
<img src="img/t5.png" width="70%">
<br>

2. 「**セッションマネージャー**」タブを選択し、「**接続**」をクリック
<img src="img/t6.png" width="70%">
<br>
3. システムパッケージのインストール  

```
sudo yum install -y lsscsi iscsi-initiator-utils sg3_utils device-mapper-multipath
```
4. マルチパスデーモンの開始
```
sudo mpathconf --enable --with_multipathd y --find_multipaths n
```
5. iSCSIとマルチパスデーモンの有効化と確認

```
sudo systemctl enable iscsid multipathd
sudo systemctl start iscsid multipathd
```
6. iSCSIの有効化と開始
```
sudo systemctl enable iscsi
sudo systemctl start iscsi
```
# Tridentインストール
## Helmの導入
Helmは最低でもバージョン3以上が必要となります。
**Cloudshell**から以下のコマンドを実行します。
```
curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash
```

## Tridentの入手
以下コマンドでTridentをダウンロードします。
```
curl -Ls https://api.github.com/repos/netapp/trident/releases/latest | jq ".assets[0].browser_download_url" | xargs -n1 curl -LOJR
```
## ONTAPストレージの確認
ONTAPストレージはクラスタの中に、仮想化されたストレージ＝SVMを作り出し、SVMを一台の独立したストレージの様に扱う事が可能です。オンプレミス環境ではこのSVMを作成する必要がありますが、FSx for NetApp ONTAPではSVMは作成済みのため、ここでは確認作業のみ実施します。**Cloud9**から操作してください。  
1. SVMの確認  
```
vserver show -vserver fsx -instance
```

2. Network Interfaceの確認  
```
network interface show
```
<img src="img/t4.png" width="70%">
<br>
※nfs_smb_managemnt_1がNAS用のData LIF兼SVM Management LIFとなります。  

3. Export Policy（アクセス許可）の確認
```
vserver export-policy rule show -policyname default
```

4. Export Policy（アクセス許可）の編集  
kubernetes（EKS）クラスタのノードIPアドレス範囲（172.31.0.0/16）からのみアクセスできるようExport Policyを編集します。
```
vserver export-policy rule modify -policyname default -ruleindex 1 -clientmatch 172.31.0.0/16
```
5. 変更を確認します。
```
vserver export-policy rule show -policyname default
```

## Kubernetes（EKS）内からのアクセス確認
ストレージとKubernetes（EKS）がネットワーク的に疎通可能かどうかを確認します。
### ノードのIPからデータLIFへの疎通確認
1. AWSマネジメントコンソール＞EC2>インスタンス＞インスタンス＞trident-eksctl-trident-eksctl-nodegroups-Nodeにチェックを入れて「**接続**」をクリック  
<img src="img/t5.png" width="70%">
<br>

2. 「**セッションマネージャー**」タブを選択し、「**接続**」をクリック
<img src="img/t6.png" width="70%">
<br>

3. 以下コマンドを入力
```
ping ［データLIFのIP］ -c 3
```
※**データLIFのIP＝nfs_smb_management_1のIP**です。**Cloud9**から`network interface show`で確認してください。
### Podの中から管理LIFへの疎通確認
Cloudshellから以下コマンドを入力します。
```
kubectl run -it --rm --image=alpine storage-test -- ping ［SVM管理LIFのIPアドレス］ -c 3
```
※**SVM管理LIFのIPアドレス=nfs_smb_managemnt_1のIP**
## Trident Operatorによる導入
**Cloudshell**から以下手順を実施。
1. tridentのリリースを展開
```
tar xzf trident-installer-23.10.0.tar.gz
```
※Tridentのバージョンが異なる場合は適宜コマンドを修正してください。

2. tridentctlコマンドをインストール 
```
sudo install -m 0755 -o root -g root ./trident-installer/tridentctl /usr/local/bin/
```
3. tridentを導入するネームスペースの作成
```
kubectl create ns trident
```
4. trident operatorのインストール
```
helm install -n trident trident trident-installer/helm/trident-operator-23.10.0.tgz
```

## Tridentインストールの確認
1.  tridentのoperatorがDeploymentとDaemonSetのコンテナを展開するのを待ちます。 
 ```
 kubectl get pod -n trident
 ``` 
 を実行して trident-csi で始まる Podが、すべてRunningになっている事を確認してください。
<img src="img/t7.png" width="70%">
<br>
2. 完全に展開が完了すると tridentctl コマンドでサーバのバージョン等を取得出来るようになります。
```
tridentctl version -n trident
```
# Tridentの設定（NAS前提）
Tridentに、制御対象のストレージにアクセスする方法を教える＝バックエンド設定 を行います。
細かい設定もできるのですが、本トレーニングでは最小限のパラメータを入れたyamlファイルを利用します。
```
cat trident-backend-nas.yml
```
でファイルを確認してください。

managementLIF: 172.31.XXX.XXX
dataLIF: 172.31.XXX.XXX
についてはご自身の環境に合わせて編集してください。  
※Cloud9からONTAP CLIで``network interface show``で確認できます。managementLIFはclustermanagentLIFではなく、SVM managementLIFです。FSx環境ではdataLIFと同一IPとなります。  

ファイルを編集したら、tridentctlを使って設定します。
```
tridentctl create backend -n trident -f trident-backend-nas.yml
```
※ ストレージが複数ある場合、複数のバックエンドを一つのTridentに登録して利用する事が可能です。

# StorageClassの設定
続いてKubernetes側でユーザからの要求とTridentが持つストレージを結びつけるために必要なStorageClassを設定します。
 
ユーザは管理者が設定したStorageClassから自分の利用条件に合うようなClassを選択してリクエスト=PersistentVolumeClaimを作成します。

```
cat storageclass-nas.yml
```
でファイルを確認してください。
```
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: nas
provisioner: csi.trident.netapp.io
parameters:
  backendType: "ontap-nas"
allowVolumeExpansion: true
volumeBindingMode: Immediate
```
確認したら、以下コマンドを実行します。
```
kubectl create --save-config -f storageclass-nas.yml
```
以下コマンドで結果を確認します。
```
kubectl get storageclass 
```
ここまでで管理者側の作業としては、ほぼ完了です。

# PersistentVolumeの利用
以下の様なPersistentVolumeClaimのマニフェストが配置されています。
```
cat pvc-nas.yml
```
```
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: nas-vol1
spec:
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 1Gi
  storageClassName: nas
```
確認したら以下コマンドでPVCを作成してください。
```
kubectl apply -f pvc-nas.yml
```
以下コマンドでPersistentVolumeが作成されたことを確認します。
```
kubectl get pvc,pv
```
## ONTAP側から確認
ONTAP側からPersistentVolumeとして利用されるFlexVolが作成されていることを確認します。  
Cloud9から以下コマンドを入力してください。
```
volume show -vserver fsx 
```
PersitentVolumeと同じ名前のFlexVolが作成されていることを確認してください。ストレージ操作なしでVolumeが作成できて便利ですね。

# Snapshotの利用
まずはCRDsとvolume snapshot controllerを展開します。
以下コマンドでvolume snapshot CRDsを作成します。
```
cat snapshot-setup.sh
#!/bin/bash
# Create volume snapshot CRDs
kubectl apply -f https://raw.githubusercontent.com/kubernetes-csi/external-snapshotter/release-6.1/client/config/crd/snapshot.storage.k8s.io_volumesnapshotclasses.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes-csi/external-snapshotter/release-6.1/client/config/crd/snapshot.storage.k8s.io_volumesnapshotcontents.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes-csi/external-snapshotter/release-6.1/client/config/crd/snapshot.storage.k8s.io_volumesnapshots.yaml
```
次にsnaoshot controllerを作ります。
```
kubectl apply -f https://raw.githubusercontent.com/kubernetes-csi/external-snapshotter/release-6.1/deploy/kubernetes/snapshot-controller/rbac-snapshot-controller.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes-csi/external-snapshotter/release-6.1/deploy/kubernetes/snapshot-controller/setup-snapshot-controller.yaml
```
### VolumeSnapshotClassを作成
```
cat snap-sc.yml
```
```
apiVersion: snapshot.storage.k8s.io/v1
kind: VolumeSnapshotClass
metadata:
  name: csi-snapclass
driver: csi.trident.netapp.io
deletionPolicy: Delete
```

```
kubectl apply -f snap-sc.yml
```
### Sanpshotを作成
```
cat snap.yml
```
```
cat snap.yml
apiVersion: snapshot.storage.k8s.io/v1
kind: VolumeSnapshot
metadata:
  name: pvc1-snap
spec:
  volumeSnapshotClassName: csi-snapclass
  source:
    persistentVolumeClaimName: nas-vol1
```
```
kubectl apply -f snap.yml
```
### Snapshotの確認
kubernetes側からSnapshotを確認します。
```
kubectl get volumesnapshots
kubectl describe volumesnapshots pvc1-snap
```
次にONTAP側からSnapshotを確認します。Cloud9から以下コマンドを入力します。
```
volume snapshot show
```
PV用のFlexVolに同じ名前のSnapshotが作成されていることを確認します。

# iSCSIでの利用
次はEKSからiSCSIでONTAPを利用します。  
## ONTAP側のiSCSI設定と確認
ONTAP側でiSCSI設定が必要ですが、FSx for NetApp ONTAPではすでに設定済みです。本トレーニングでは確認のみ実施します。**Cloud9**から以下コマンドを入力します。  
1. iSCSI用IPの確認
```
network interface show
```
2. iSCSI設定の確認
```
iscsi show
iscsi initiator show 
iscsi interface show
```

## TridentでのiSCSI設定
以下手順は**CloudShell**から実施してください。
1. Backendの設定
```
cat trident-backend-san.yml
```
でファイルを確認してください。

managementLIF: 172.31.XXX.XXX
についてはご自身の環境に合わせて編集してください。

```
tridentctl create backend -n trident -f trident-backend-san.yml
```

2. StorageClassの設定
iSCSI用のStorageClassを作成します。
```
cat storageclass-san.yml
```
```
kubectl create --save-config -f storageclass-san.yml
kubectl get storageclass 
```
 
***
## iSCSI用PVCとPodの作成
1. iSCSI用PVCを作成します。
```
cat pvc-iscsi.yml
```
```
kubectl apply -f pvc-iscsi.yml
kubectl get pvc
```

2. 作成したiSCSI用PVCをマウントするPodを作成します。
```
cat pod-iscsi.yml
```
```
kubectl apply -f pod-iscsi.yml
```

3. 再度ONTAP側でiscsi設定を確認。**Cloud9**で以下コマンドを入力します。
```
iscsi show
iscsi initiator show 
iscsi interface show
iscsi connection show
iscsi session show
```
# 後片付け
**Cloudshell**で以下コマンドを入力してリソースを削除します。
```
kubectl delete -f pod-iscsi.yml
kubectl delete -f pvc-nas.yml
kubectl delete -f pvc-iscsi.yml
```
**Cloud9**から以下のONTAP CLIコマンドを入力します。
```
vol show
```
tident_pvc_XXXXXというVolumeが削除されていることを確認してください。  
ついでに下記コマンドでデフォルトで作成されているvol1を削除していただけると助かります。
```
vol offline vol1
```
**y**を入力
```
vol delete vol1
```
**y**を入力  
※fsx_rootのVolumeはそのままでかまいません。

**Cloudshell**で以下コマンドを入力してEKSを削除します。
```
eksctl delete cluster -f trident-handson-eks.yml
```
以上で終了です。お疲れ様でした。 

